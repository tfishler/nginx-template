#!/usr/bin/env bash

source $(dirname $0)/config
if [[ -z ${DOMAIN} ]]; then
	OLDIFS=$IFS
	IFS=","
	DOMAIN=$1
fi
	
[[ -d ${NGINX_AVAILABLE} ]] || mkdir -p ${NGINX_AVAILABLE}
[[ -d ${NGINX_ENABLED} ]] || mkdir -p ${NGINX_ENABLED}
[[ -d ${NGINX_SSL_DIR} ]] || mkdir -p ${NGINX_SSL_DIR}


function generate_certs { 
	DOMAIN=$1
	[[ -f ${NGINX_SSL_DIR}/dhparam.pem ]] || openssl dhparam -out ${NGINX_SSL_DIR}/dhparam.pem 2048
	openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
	-keyout ${NGINX_SSL_DIR}/${DOMAIN}.key \
	-out ${NGINX_SSL_DIR}/${DOMAIN}.crt \
	-subj "/C=${SSL_C}/ST=${SSL_ST}/L=${SSL_L}/O=${SSL_O}/CN=${DOMAIN}"
}

function render_template() {
	eval "echo \"$(cat $1)\""
}

function generate_conf {
	DOMAIN=$1
	echo "#### Creating ${NGINX_AVAILABLE}/${DOMAIN}.conf from template $(dirname $0)/nginx.conf.tmpl"
	render_template $(dirname $0)/nginx.conf.tmpl > ${NGINX_AVAILABLE}/${DOMAIN}.conf
}

function make_sym_links { 
	DOMAIN=$1
	sudo ln -sf ${NGINX_AVAILABLE}/${DOMAIN} ${NGINX_ENABLED}/${DOMAIN}
} 

for domain in "${DOMAIN[@]}"; do
	generate_certs ${domain}
	generate_conf ${domain}
	make_sym_links ${domain}.conf 
done

IFS=$OLDIFS
systemctl restart nginx.service
